import React, { useState } from "react";
import "./JoinRoom.css";
import { Avatar } from "@material-ui/core";
import { db } from "../../firebase";
import { useStateValue } from "../../StateProvider";

const JoinRoom = () => {
  const [roomId, setroomId] = useState("");
  const [{ user }] = useStateValue();

  const joinRoom = (e) => {
    e.preventDefault();
    console.log(user);

    db.collection("users")
      .doc(user.uid)
      .collection("joinedRoom")
      .where("roomId", "==", roomId)
      .get()
      .then((res) => {
        console.log(res.docs);
        if (res.docs[0]) {
          alert("Anda sudah tergabung dalam room ini");
        } else {
          const docRooms = db
            .collection("rooms")
            .get()
            .then((doc) => {
              doc.docs.map((dt) => {
                if (dt.id == roomId) {
                  db.collection("users")
                    .doc(user.uid)
                    .collection("joinedRoom")
                    .add({
                      roomId: dt.id,
                      roomName: dt.data().roomName,
                    });
                  db.collection("rooms")
                    .doc(dt.id)
                    .collection("usersJoined")
                    .add({
                      userId: user.uid,
                      userName: user.displayName,
                    });
                }
              });
            })
            .catch((err) => console.log("Salah"));
        }
      });

    document.getElementById("roomId").value = "";
  };

  return (
    <div className="createRoom">
      <h2>Join Room</h2>
      <Avatar />
      <div className="createRoom__form">
        <form onSubmit={(e) => joinRoom(e)}>
          <div className="createRoom__inpContainer">
            <input
              type="text"
              id="roomId"
              onChange={(e) => setroomId(e.target.value)}
              placeholder="Room Id"
              required
            />
          </div>
          <button type="submit">Join</button>
        </form>
      </div>
    </div>
  );
};

export default JoinRoom;
