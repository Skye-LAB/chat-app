import React, { Component } from "react";
import "./StarterChat.css";
import ChatIcon from "@material-ui/icons/Chat";

const StarterChat = () => {
  return (
    <div className="starterChat">
      <div className="startChat__info">
        <ChatIcon />
        <p>Join a Room or Make a New One!</p>
      </div>
    </div>
  );
};

export default StarterChat;

