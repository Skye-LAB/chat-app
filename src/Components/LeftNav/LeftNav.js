import React from "react";
import "./LeftNav.css";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { SearchOutlined } from "@material-ui/icons";
import { Avatar, IconButton } from "@material-ui/core";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import LeftNavChat from "../LeftNavChat/LeftNavChat";
import { auth } from "../../firebase";
import { useStateValue } from "../../StateProvider";
const LeftNav = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [{user}, dispatch] = useStateValue();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const createRoom = () => {
    dispatch({
      type: "CREATE_NEW",
      status: "CREATE_ROOM",
    });
    setAnchorEl(null);
  };
  const joinRoom = () => {
    dispatch({
      type: "JOIN",
      status: "JOIN_ROOM",
    });
    setAnchorEl(null);
  };

  const signOut = () => {
    auth.signOut();
    dispatch({
      type: "CLEAR_ALL",
      status: "EMPTY",
    });
  };


  return (
    <div className="leftNav">
      <div className="leftNav__header">
        <Avatar />
        <p>{user.displayName}</p>
        <div className="leftNav__headerRight">
          <IconButton onClick={handleClick}>
            <MoreVertIcon />
          </IconButton>
        </div>
        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose}>Profile</MenuItem>
          <MenuItem onClick={createRoom}>Create Room</MenuItem>
          <MenuItem onClick={joinRoom}>Join Room</MenuItem>
          <MenuItem onClick={signOut}>Logout</MenuItem>
        </Menu>
      </div>
      <div className="leftNav__search">
        <div className="leftNav__searchContainer">
          <SearchOutlined />
          <input type="text" placeholder="Search Room"/>
        </div>
      </div>
      <div className="leftNav__chats">
        <LeftNavChat />
      </div>
    </div>
  );
};

export default LeftNav;
