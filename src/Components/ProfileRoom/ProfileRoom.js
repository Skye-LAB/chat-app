import React, { useEffect, useState } from "react";
import "./ProfileRoom.css";

import { Avatar } from "@material-ui/core";
import {db} from '../../firebase'
import { useStateValue } from "../../StateProvider";

const ProfileRoom = () => {
  const [{ roomId, roomName, user }, dispatch] = useStateValue();
  const [usersJoined, setusersJoined] = useState([])
  const [newRoomName, setnewRoomName] = useState('')

  useEffect(() => {
    db.collection('rooms').doc(roomId).collection('usersJoined').orderBy('userName', 'asc').get().then(doc=>{
      setusersJoined(
        doc.docs.map(data=>({
          id: data.id,
          data : data.data()
        }))
        )
    })
  }, [])

  const EditRoomName = (status)=>{
    if(status == 'edit'){
      document.getElementById('roomName').style.display ='none';
      document.getElementById('editRoomName').style.display ='none';

      document.getElementById('newRoomName').style.display ='block';
      document.getElementById('changeRoomName').style.display ='block';
    }
  }
  
  const ChangeRoomName = ()=>{

    if(newRoomName){
      db.collection('rooms').doc(roomId).set({
        roomName : newRoomName
      })
      
      db.collection('users').doc(user.uid).collection('joinedRoom').where('roomId', '==', roomId).get().then(doc=>{
        db.collection('users').doc(user.uid).collection('joinedRoom').doc(doc.docs[0].id).set({
          roomId : roomId,
          roomName : newRoomName
        })
      })

      dispatch({
        type: "NEW_ROOM_NAME",
        roomName: newRoomName,
      });
      
      document.getElementById('roomName').style.display ='block';
      document.getElementById('editRoomName').style.display ='block';
    
      document.getElementById('newRoomName').style.display ='none';
      document.getElementById('changeRoomName').style.display ='none';
    }

  }

  return (
    <div className="profileRoom">
      <div className="profileRoom__header">
        <Avatar />
        <div className="profileRoom__info">
          <h3>{roomName}</h3>
          <p>{roomId}</p>
        </div>
      </div>
      <div className="profileRoom__body">
        <div className="wrapper">
          <div className="profileRoom__title_info" id="info">
            <Avatar />
            <h2 id='roomName'>{roomName} </h2>
            <button id='editRoomName' onClick={()=>EditRoomName('edit')} >edit</button>  
            <input type='text' id='newRoomName' onChange={e=>setnewRoomName(e.target.value)} />
            <button id='changeRoomName' onClick={()=>ChangeRoomName()} >change</button>  
          </div>
        </div>
        <div className="wrapper">
          <div className="profileRoom_list_user">
            <h3>Users List</h3>
            <hr />
            {
            usersJoined.map((u) => {
              return (
                <div className="userOne" key={u.data.userId}>
                  <Avatar />
                  <div className="userInfo">
                    <p>{u.data.userName}</p>
                    {/* <p>{u.data.userId}</p> */}
                  </div>
                </div>
              );
            })
            }
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfileRoom;
