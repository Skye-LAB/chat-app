import React, { useState } from "react";
import "./Login.css";

// Material UI
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import { useHistory } from "react-router-dom";
import { auth } from "../../firebase";
import { useStateValue } from "../../StateProvider";

const useStyle = makeStyles({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  cardContent: {
    width: "300px",
    height: "fit-content",
    display: "flex",
    flexDirection: "column",
    padding: "20px",
  },
  inp: {
    width: "100%",
    paddingBottom: "10px",
  },
});

const Login = () => {
  const classes = useStyle();
  const [{}, dispatch] = useStateValue();
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const signIn = (e) => {
    e.preventDefault();

    auth
      .signInWithEmailAndPassword(email, password)
      .then((auth) => history.push("/"))
      .catch((err) => alert(err.message));
  };

  const goRegist = (e) => {
    e.preventDefault();

    dispatch({
      type: "GO_REGISTER",
      status: "REGISTER",
    });
  };

  return (
    <div className={classes.root}>
      <Card variant="outlined" className={classes.cardContent}>
        <CardContent>
          <h2>Login</h2>
          <TextField
            id="filled-basic"
            label="Email"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className={classes.inp}
          />
          <TextField
            id="filled-basic"
            label="Password"
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className={classes.inp}
          />
          <Button
            variant="contained"
            color="primary"
            style={{ width: "100%" }}
            onClick={signIn}
          >
            Sign In
          </Button>
          <span>
            Do not have an account?
            <a href="" onClick={goRegist}>
              Register
            </a>
          </span>
        </CardContent>
      </Card>
    </div>
  );
};

export default Login;
