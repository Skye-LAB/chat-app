import React from "react";
import "./Home.css";

// Component
import Login from "../Login/Login";
import Register from "../Register/Register";
// Material UI
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

import { useStateValue } from "../../StateProvider";
import { useHistory } from "react-router-dom";
import { auth, provider, db } from "../../firebase";

const Home = () => {
  const history = useHistory();
  const [{ status }] = useStateValue();

  const googleAuth = (e) => {
    e.preventDefault();
    auth
      .signInWithPopup(provider)
      .then((auth) => {
        db.collection("users").doc(auth.user.uid).set({
          name: auth.user.displayName,
        });
        history.push("/");
      })
      .catch((err) => console.log("Please click the button again"));
  };

  return (
    <div className="home">
      <div className="container">
        <Grid
          container
          direction="column"
          alignItems="center"
          justify="center"
          style={{ minHeight: "100vh" }}
        >
          <Grid style={{ marginBottom: "20px" }}>
            <img src="./img/logo.png" alt="" width="200" />
          </Grid>
          <Grid item xs={12}>
            {(() => {
              switch (status) {
                case "LOGIN":
                  return <Login />;
                case "REGISTER":
                  return <Register />;
                default:
                  return <Login />;
              }
            })()}
          </Grid>
          <Grid item xs={12} style={{ paddingTop: "20px" }}>
            <Button variant="contained" onClick={googleAuth} style={{ backgroundColor: "white", color: "black" }}>
              <img src="https://www.freepnglogos.com/uploads/google-logo-png/google-logo-png-webinar-optimizing-for-success-google-business-webinar-13.png" width="30"/> &nbsp;
              Google
            </Button>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default Home;
