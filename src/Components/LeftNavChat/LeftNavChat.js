import React, { useEffect, useState } from "react";
import "./LeftNavChat.css";
import { Avatar } from "@material-ui/core";

import { useStateValue } from "../../StateProvider";

import { db } from "../../firebase";

const LeftNavChat = () => {
  const [{ user }, dispatch] = useStateValue();
  const [Rooms, setRooms] = useState([]);

  useEffect(() => {
    db.collection("users").doc(user.uid).collection("joinedRoom").onSnapshot(doc =>{
      setRooms(
        doc.docs.map((dt) => ({
          id: dt.id,
          data: dt.data()
        }))
      )
    })
  }, []);

  const roomChat = (id,name) => {
    dispatch({
      type: "OPEN_CHAT",
      roomId: id,
      roomName: name,
      status: "CHAT",
    });
  };

  const LastMsg =(id)=>{
    db.collection("rooms").doc(id).collection('msgs').orderBy('time','desc').limit(1).onSnapshot(doc=> {
      if(doc.docs[0]) {
        let name = doc.docs[0].data().Username.split(' ');
        if(doc.docs[0].data().msg.length > 35 ){
          let msg = doc.docs[0].data().msg.substring(0,35);
          document.getElementById(id).innerHTML = name[0] +' : '+ msg+'....' ;
        }else{
          document.getElementById(id).innerHTML = name[0] +' : '+ doc.docs[0].data().msg;
        }
      }
    })
  }

  return (
    <div>
      {
        Rooms.map((room) => {
          return (
            <div key={room.id}>
              <div
                className="leftNavChat"
                onClick={() => roomChat(room.data.roomId, room.data.roomName)}
              >
                <Avatar />
                <div className="leftNav__info">
                  <h2>{room.data.roomName}</h2>
                  <p id={room.data.roomId}></p>
                  {LastMsg(room.data.roomId)} 
                </div>
              </div>
            </div>
          );
        })
      }
    </div>
  );
};

export default LeftNavChat;
