import React, { useState, useEffect, useRef } from "react";
import "./Chat.css";
import {
  Avatar,
  IconButton,
  Portal,
  Menu,
  MenuItem,
  TextareaAutosize,
} from "@material-ui/core";
import { MoreVert, InsertEmoticon, Send, Settings } from "@material-ui/icons";
import { db } from "../../firebase";
import { useStateValue } from "../../StateProvider";
var unirest = require("unirest");
const axios = require('../../axios')

const Chat = () => {
  const [Input, setInput] = useState("");
  const [Chats, setChats] = useState([]);
  const [Users, setUsers] = useState([]);
  const [OpenTrans, setOpenTrans] = useState({});
  const [anchorEl, setAnchorEl] = React.useState(null);

  const [show, setShow] = React.useState(true);
  const container = React.useRef(null);

  const [
    { user, roomId, roomName, detectApi, translateApi, headerApi },
    dispatch,
  ] = useStateValue();

  let a = 0;

  const handleClick = () => {
    setShow(!show);
  };
  const handleAnchor = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const openProfileRoom = () => {
    dispatch({
      type: "GOTO_PROFILE_ROOM",
      status: "PROFILE_ROOM",
      roomId: roomId,
      roomName: roomName,
    });
    setAnchorEl(null);
  };

  const exitroom = () => {
    setAnchorEl(null);
    db.collection("rooms")
      .doc(roomId)
      .collection("usersJoined")
      .get()
      .then((doc) => {
        if (doc.docs.length == 1) {
          alert("This room will be deleted");
          const id = prompt("insert the room Id if you sure :");
          if (id == roomId) {
            db.collection("rooms")
              .doc(roomId)
              .collection("usersJoined")
              .where("userId", "==", user.uid)
              .get()
              .then((doc) => {
                db.collection("rooms")
                  .doc(roomId)
                  .collection("usersJoined")
                  .doc(doc.docs[0].id)
                  .delete();
              });
            db.collection("rooms")
              .doc(roomId)
              .collection("msgs")
              .get()
              .then((doc) => {
                if (doc.docs[0]) {
                  doc.docs.map((dt) => {
                    db.collection("rooms")
                      .doc(roomId)
                      .collection("msgs")
                      .doc(dt.id)
                      .delete();
                  });
                }
              });
            db.collection("rooms").doc(roomId).delete();

            db.collection("users")
              .doc(user.uid)
              .collection("joinedRoom")
              .where("roomId", "==", roomId)
              .get()
              .then((doc) => {
                db.collection("users")
                  .doc(user.uid)
                  .collection("joinedRoom")
                  .doc(doc.docs[0].id)
                  .delete();
                dispatch({
                  type: "EXIT_ROOM",
                  status: "EMPTY",
                });
              });
          }
        } else {
          const id = prompt("insert the room Id for validation :");
          if (id == roomId) {
            db.collection("rooms")
              .doc(roomId)
              .collection("usersJoined")
              .where("userId", "==", user.uid)
              .get()
              .then((doc) => {
                db.collection("rooms")
                  .doc(roomId)
                  .collection("usersJoined")
                  .doc(doc.docs[0].id)
                  .delete();
              });

            db.collection("users")
              .doc(user.uid)
              .collection("joinedRoom")
              .where("roomId", "==", roomId)
              .get()
              .then((doc) => {
                db.collection("users")
                  .doc(user.uid)
                  .collection("joinedRoom")
                  .doc(doc.docs[0].id)
                  .delete();

                dispatch({
                  type: "EXIT_ROOM",
                  status: "EMPTY",
                });
              });
          }
        }
      });
  };

  useEffect(() => {
    const docRef = db.collection("rooms").doc(roomId);
    docRef
      .collection("msgs")
      .orderBy("time", "asc")
      .onSnapshot((doc) =>
        setChats(
          doc.docs.map((dt) => ({
            id: dt.id,
            data: dt.data(),
          }))
        )
      );
    docRef
      .collection("usersJoined")
      .orderBy("userName", "asc")
      .get()
      .then((e) => {
        setUsers(
          e.docs.map((dt) => ({
            id: dt.id,
            name: dt.data().userName,
          }))
        );
      });
  }, [roomId]);

  useEffect(() => {
    document
      .getElementById("chatBody")
      .scrollTo(0, document.getElementById("chatBody").scrollHeight);
  },[Chats]);

  const sendChat = (e) => {
    e.preventDefault();

    const to = document.getElementById("to").value;
    if (to) {
      translate("input", Input, "");
    } else {
      db.collection("rooms").doc(roomId).collection("msgs").add({
        msg: Input,
        time: new Date(),
        userId: user.uid,
        Username: user.displayName,
      });
    }
    document.getElementById("input").value = "";
  };

  const translate = (type = "", msg = "", id = "") => {
    var to = "";
    var translated;
    if (type == "chat") {
      to = document.getElementById(`to${id}`).value;
    } else {
      to = document.getElementById("to").value;
    }

    axios.post('/detect', {data:{text:msg}}).then(result=>{
      console.log(result.data.from,"detect");
      if(to == result.data.from){
        translated = msg;
      }else{
        axios.post('/trans', {data:{text: msg }, to: to }).then(translated =>{
        })
        
      }
      if (type === "chat") { 
        console.log("2");
        if( OpenTrans[id] == null){
          OpenTrans[id] = true;
        }else if(OpenTrans[id] != null){
          OpenTrans[id] = !OpenTrans[id];
        }

      if (OpenTrans[id] == true) {
          document.getElementById(`translateOutPut${id}`).style.height = "fit-content";
          document.getElementById(`translateOutPut${id}`).style.padding = "10px";
          document.getElementById(`translateOutPut${id}`).innerHTML = translated;
      } else {
          if (id) {
            document.getElementById(`translateOutPut${id}`).style.height = "0";
            document.getElementById(`translateOutPut${id}`).style.padding = "0";
            document.getElementById(`translateOutPut${id}`).innerHTML = null;
          } else {
            console.log("no_id");
          }
      }
      }else{
        db.collection("rooms").doc(roomId).collection("msgs").add({
          msg: translated,
          time: new Date(),
          userId: user.uid,
          Username: user.displayName,
        });
      }
    })
  };

  return (
    <div className="chat">
      <div className="chat__header">
        <Avatar />
        <div className="chat__info">
          <h3>{roomName}</h3>
          <div className="lastSeen">
            {Users.map((user) => {
              if (Users.length - 1 !== a) {
                a++;
                return <span key={user.id}>{user.name + ", "}</span>;
              } else {
                return <span key={user.id}>{user.name}</span>;
              }
            })}
          </div>
        </div>

        <div className="chat__headerRight">
          <IconButton onClick={handleAnchor}>
            <MoreVert />
          </IconButton>
          <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={openProfileRoom}>Profile Room</MenuItem>
            <MenuItem onClick={exitroom}>Exit Room</MenuItem>
          </Menu>
        </div>
      </div>
      <div className="chat__body" id="chatBody">
        {Chats.map((chat) => {
          if (chat.data.userId == user.uid) {
            return (
              <div key={chat.id}>
                <p className="chat__message chat__receiver">
                  <span className="chat__name">{chat.data.userName}</span>
                  {chat.data.msg}
                  <span>
                    <select id={"to" + chat.id}>
                      <option value="en">English</option>
                      <option value="id">Indonesia</option>
                      <option value="ja">Japanese</option>
                      <option value="zh">Chinese</option>
                      <option value="tr">Turkish</option>
                    </select>
                    <button
                      type="button"
                      onClick={() => translate("chat", chat.data.msg, chat.id)}
                    >
                      translate
                    </button>
                  </span>
                  <span className="chat__timestamp">
                    {new Date(chat.data.time.toDate()).toLocaleString("id-ID", {
                      hour: "2-digit",
                      minute: "2-digit",
                      hour12: true,
                    })}
                  </span>
                </p>
                <div
                  className="translate__msg gst"
                  id={"translateOutPut" + chat.id}
                ></div>
              </div>
            );
          } else {
            return (
              <div key={chat.id}>
                <p className="chat__message">
                  <span className="chat__name">{chat.data.Username}</span>
                  {chat.data.msg}
                  <br />
                  <span>
                    <select id={"to" + chat.id}>
                      <option value="en">English</option>
                      <option value="id">Indonesia</option>
                      <option value="ja">Japanese</option>
                      <option value="zh">Chinese</option>
                      <option value="tr">Turkish</option>
                    </select>
                    <button
                      type="button"
                      onClick={() => translate("chat", chat.data.msg, chat.id)}
                    >
                      translate
                    </button>
                  </span>
                  <span className="chat__timestamp">
                    {new Date(chat.data.time.toDate()).toLocaleString("id-ID", {
                      hour: "2-digit",
                      minute: "2-digit",
                      hour12: true,
                    })}
                  </span>
                </p>
                <div
                  className="translate__msg"
                  id={"translateOutPut" + chat.id}
                ></div>
              </div>
            );
          }
        })}
      </div>
      <div className="chat__footer">
        <form onSubmit={(e) => sendChat(e)}>
          {show ? (
            <Portal container={container.current}>
              <div className="sel">
                <select name="to" id="to">
                  <option value="">Translate To</option>
                  <option value="en">English</option>
                  <option value="id">Indonesia</option>
                  <option value="ja">Japanese</option>
                  <option value="zh">Chinese</option>
                  <option value="tr">Turkish</option>
                </select>
              </div>
            </Portal>
          ) : null}
          <div className="trans" ref={container}></div>
          <div className="chat__ops">
            <div className="chat__inputContainer">
              <TextareaAutosize
                id="input"
                style={{ resize: "none" }}
                onChange={(e) => setInput(e.target.value)}
                autoComplete="off"
                aria-label="empty textarea"
                placeholder="Message"
              />
              <IconButton>
                <InsertEmoticon />
              </IconButton>
              <IconButton onClick={handleClick}>
                <Settings />
              </IconButton>
            </div>
            <IconButton onClick={(e) => sendChat(e)}>
              <Send />
            </IconButton>
            <button type="submit" className="btn">
              Send
            </button>
          </div>
        </form>
        <button type="button" onClick={()=>console.log(OpenTrans)} >log</button>
      </div>
    </div>
  );
};

export default Chat;
