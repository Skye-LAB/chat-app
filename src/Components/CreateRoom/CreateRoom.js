import React, { useState } from "react";
import "./CreateRoom.css";
import { Avatar } from "@material-ui/core";
import { db } from "../../firebase";
import { useStateValue } from "../../StateProvider";

const CreateRoom = () => {
  const [roomName, setroomName] = useState("");
  const [{ user }] = useStateValue();

  const makeRoom = (e) => {
    e.preventDefault();

    const docRooms = db.collection("rooms");
    docRooms
      .add({
        roomName: roomName,
      })
      .then((e) => {
        docRooms.doc(e.id).collection("usersJoined").add({
          userId: user.uid,
          userName: user.displayName,
        });
      });
    docRooms
      .where("roomName", "==", roomName)
      .get()
      .then((doc) => {
        db.collection("users").doc(user.uid).collection("joinedRoom").add({
          roomId: doc.docs[0].id,
          roomName: roomName,
        });
      });

    document.getElementById("roomName").value = "";
  };

  return (
    <div className="createRoom">
      <h2>Create Room</h2>
      <Avatar />
      <div className="createRoom__form">
        <form onSubmit={(e) => makeRoom(e)}>
          <div className="createRoom__inpContainer">
            <input
              type="text"
              id="roomName"
              onChange={(e) => setroomName(e.target.value)}
              placeholder="Room Name"
              required
            />
          </div>
          <button type="submit">Create</button>
        </form>
      </div>
    </div>
  );
};

export default CreateRoom;
