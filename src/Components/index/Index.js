import React from "react";
import "./Index.css";

// Component
import LeftNav from "../LeftNav/LeftNav";
import Chat from "../Chat/Chat";
import CreateRoom from "../CreateRoom/CreateRoom";
import JoinRoom from "../JoinRoom/JoinRoom";
import StarterChat from "../StarterChat/StarterChat";
import ProfileRoom from "../ProfileRoom/ProfileRoom";
import { useStateValue } from "../../StateProvider";

const Index = () => {
  const [{ status }] = useStateValue();

  return (
    <div className="index">
      <div className="index__body">
        <LeftNav />
        {(() => {
          switch (status) {
            case "EMPTY":
              return <StarterChat />;
            case "CREATE_ROOM":
              return <CreateRoom />;
            case "JOIN_ROOM":
              return <JoinRoom />;
            case "CHAT":
              return <Chat />;
            case "PROFILE_ROOM":
              return <ProfileRoom />;
            default:
              return <StarterChat />;
          }
        })()}
      </div>
    </div>
  );
};

export default Index;
