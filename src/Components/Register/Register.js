import React, { useState } from "react";
import "./Register.css";

// Material UI
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import { useHistory } from "react-router-dom";
import { auth, db } from "../../firebase";
import { useStateValue } from "../../StateProvider";

const useStyle = makeStyles({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  cardContent: {
    width: "300px",
    height: "fit-content",
    display: "flex",
    flexDirection: "column",
    padding: "20px",
  },
  inp: {
    width: "100%",
    paddingBottom: "10px",
  },
});

const Register = () => {
  const classes = useStyle();
  const [{}, dispatch] = useStateValue();
  const history = useHistory();
  const [displayName, setDisplayName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const register = (e) => {
    e.preventDefault();

    auth
      .createUserWithEmailAndPassword(email, password)
      .then((auth) => {
        db.collection("users").doc(auth.user.uid).set({
          name: displayName,
        });
      })
      .then(() => {
        var authUser = auth.currentUser;
        authUser.updateProfile({
          displayName: displayName,
        });
      })
      .catch((err) => alert(err.message));

    if (auth) {
      history.push("/");
    }
  };

  const goLogin = (e) => {
    e.preventDefault();

    dispatch({
      type: "GO_LOGIN",
      status: "LOGIN",
    });
  };

  return (
    <div className={classes.root}>
      <Card variant="outlined" className={classes.cardContent}>
        <CardContent>
          <h2>Register</h2>
          <TextField
            id="filled-basic"
            label="Name"
            type="text"
            value={displayName}
            onChange={(e) => setDisplayName(e.target.value)}
            className={classes.inp}
          />
          <TextField
            id="filled-basic"
            label="Email"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className={classes.inp}
          />
          <TextField
            id="filled-basic"
            label="Password"
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            className={classes.inp}
          />
          <Button
            variant="contained"
            color="primary"
            style={{ width: "100%" }}
            onClick={register}
          >
            Sign Up
          </Button>
          <span>
            Already have an account?
            <a href="" onClick={goLogin}>
              Login
            </a>
          </span>
        </CardContent>
      </Card>
    </div>
  );
};

export default Register;
