import React, { useEffect } from "react";
import "./App.css";
import Home from "./Components/Home/Home";
import Index from "./Components/index/Index";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { auth } from "./firebase";
import { useStateValue } from "./StateProvider";

function App() {
  const [{ user }, dispatch] = useStateValue();

  useEffect(() => {
    auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        dispatch({
          type: "SET_USER",
          user: authUser,
        });
      } else {
        dispatch({
          type: "SET_USER",
          user: null,
        });
      }
    });
  }, []);

  return (
    <Router>
      <div className="app">
        <Switch>
          <Route path="/" exact>
            {!user ? <Home /> : <Index />}
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
