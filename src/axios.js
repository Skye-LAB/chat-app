const axios = require("axios");

const instance = axios.create({
  baseURL: "http://localhost:5001/app-chat-cfd76/us-central1/api",
});

module.exports = instance;
