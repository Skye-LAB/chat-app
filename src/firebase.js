import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyCyDsF9E6tC4R74HRzl-mi_yjFEMyegyz8",
  authDomain: "app-chat-cfd76.firebaseapp.com",
  databaseURL: "https://app-chat-cfd76.firebaseio.com",
  projectId: "app-chat-cfd76",
  storageBucket: "app-chat-cfd76.appspot.com",
  messagingSenderId: "50948531617",
  appId: "1:50948531617:web:55af097efa2143508f97e0",
  measurementId: "G-EXDTEX6SKG",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider()

export { db, auth, provider };
