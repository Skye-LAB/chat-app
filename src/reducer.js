import unirest from "unirest";

export const initialState = {
  user: null,
  status: "EMPTY",
  roomId: null,
  roomName: null,
  detectApi: unirest(
    "POST",
    "https://google-translate1.p.rapidapi.com/language/translate/v2/detect"
  ),
  translateApi: unirest(
    "POST",
    "https://google-translate1.p.rapidapi.com/language/translate/v2"
  ),
  headerApi: {
    "x-rapidapi-host": "google-translate1.p.rapidapi.com",
    "x-rapidapi-key": "67094666ccmsh98708592fee6376p18b2d7jsn8933f3d3bc16",
    "accept-encoding": "application/gzip",
    "content-type": "application/x-www-form-urlencoded",
    useQueryString: true,
  },
};

const reducer = (state, action) => {
  switch (action.type) {
    case "CREATE_NEW":
      return {
        ...state,
        status: action.status,
      };
    case "JOIN":
      return {
        ...state,
        status: action.status,
      };

    case "SET_USER":
      return {
        ...state,
        user: action.user,
      };

    case "OPEN_CHAT":
      return {
        ...state,
        roomId: action.roomId,
        roomName: action.roomName,
        status: action.status,
      };

    case "CLEAR_ALL":
      return {
        ...state,
        status: action.status,
      };

    case "GO_REGISTER":
      return {
        ...state,
        status: action.status,
      };

    case "GO_LOGIN":
      return {
        ...state,
        status: action.status,
      };

    case "GOTO_PROFILE_ROOM":
      return {
        ...state,
        status: action.status,
        roomId: action.roomId,
        roomName: action.roomName,
      };

    case "NEW_ROOM_NAME":
      return {
        ...state,
        roomName: action.roomName,
      };

    case "EXIT_ROOM":
      return {
        ...state,
        status: action.status,
      };

    default:
      return state;
  }
};

export default reducer;
