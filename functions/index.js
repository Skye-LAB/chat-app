const functions = require("firebase-functions");
const axios = require("../src/axios");
const express = require("express");
const cors = require("cors");

const LanguageTranslatorV3 = require("ibm-watson/language-translator/v3");
const { IamAuthenticator } = require("ibm-watson/auth");

const app = express();

app.use(cors());
app.use(express.json());

const languageTranslator = new LanguageTranslatorV3({
  version: "2018-05-01",
  authenticator: new IamAuthenticator({
    apikey: "4Nm_TmMpwwxyoTLHC9ePkbQ4IIcYXx1vTFzXomdsPcq1",
  }),
  serviceUrl:
    "https://api.jp-tok.language-translator.watson.cloud.ibm.com/instances/d2f53588-c0df-47b7-b3bb-70aababbe6b1",
});

app.post("/detect", (req, res) => {
  languageTranslator
    .identify(req.body.data)
    .then((identifiedLanguages) => {
      return res.status(200).json({
        from: identifiedLanguages.result.languages[0].language,
      });
    })
    .catch((err) => {
      res.status(400).json({ error: err });
      console.log("error:", err);
    });
});

app.post("/trans", (req, res) => {
    languageTranslator
      .translate({
        text: req.body.text,
        modelId: `${req.body.from}-${req.body.to}`,
      })
      .then((translationResult) => {
        return res
          .status(200)
          .json(translationResult.result.translations[0].translation);
      })
      .catch((err) => {
        console.log("error:", err);
      });
});

exports.api = functions.https.onRequest(app);
