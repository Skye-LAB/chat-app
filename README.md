# C-Trans

- [Dependency](#dependency)
- [Installation](#install)
- [Guide](#guide)
  - [Translate](#guide_1)
  - [Error](#error)
- [Overview](#overview)

<a name="dependency"></a>

## Dependency

1. [NodeJS](https://nodejs.org)
2. Akun [RapidAPI](https://rapidapi.com), untuk petunjuk mendaftar [disini](#tutor)

<a name="install"></a>

## Installation

1. Clone repository ini

```
git clone https://gitlab.com/Skye-LAB/chat-app.git
```

2. Setelah itu, ketik command ini:

```
cd chat-app
npm i or yarn
npm start or yarn start
```

<a name="guide"></a>

## Guide

<a name="guide_1"></a>

### 1. Translate

1. Ketika ingin mengirim pesan, jika ingin menerjemahkan pesan tersebut seilahkan pilih bahasa terlebih dahulu.  
   ![translate_to](./public/img/md/tutor/translate_1.jpeg)
2. Jika ingin menerjemahkan pesan yang dikirim oleh pemngguna lain, silahkan pilih opsi bahasa dan terkan translate.  
   ![translate](./public/img/md/tutor/translate_2.jpeg)
   > Jika ingin menutup pesan yang telah diterjemahkan, silahkan tekan translate kembali.

<a name="error"></a>

### 2. Error

Jika mendapat error `429 (to many request)`, seperti ini:
![Error](./public/img/md/error_429.jpeg)

<a name="tutor"></a>

1. Pertama Anda pergi ke [laman ini](https://rapidapi.com/googlecloud/api/google-translate1), dan mendaftar akun baru disana.  
   ![daftar](./public/img/md/tutor/1.jpg)
2. lalu klik tab Pricing
   ![price](./public/img/md/tutor/2.jpg)
3. Kemudian klik `Select plan` di tab Basic
   ![select_basic](./public/img/md/tutor/3.jpg)
4. Copy `apikey` yang diberikan
   ![apikey](./public/img/md/tutor/4.jpg)

> pastekan di `src/reducer.js`, pada bagian:

```js
headerApi: {
  "x-rapidapi-host": "google-translate1.p.rapidapi.com",
  "x-rapidapi-key": "abcxxxxxxxxx",
  "accept-encoding": "application/gzip",
  "content-type": "application/x-www-form-urlencoded",
  useQueryString: true,
},
```

> Diganti dibagian:

```diff
- "x-rapidapi-key": "abcxxxxxxxxx",
+ "x-rapidapi-key": "apikey yang didapat dari mendaftar tadi",

```

<a name="overview"></a>

## Overview

![preview_1](./public/img/md/preview/login.jpeg)
![preview_2](./public/img/md/preview/room.jpeg)
![preview_3](./public/img/md/preview/create.jpeg)
